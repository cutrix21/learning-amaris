<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle frontpage.
 *
 * @package    core
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!file_exists('./config.php')) {
    header('Location: install.php');
    die;
}

require_once('config.php');
require_once($CFG->dirroot .'/course/lib.php');
require_once($CFG->libdir .'/filelib.php');

redirect_if_major_upgrade_required();

if (isset($_GET['abon'])) {
    require_once('src/PHPMailer.php');

    try {


        $mail = new PHPMailer\PHPMailer\PHPMailer();

        //$to = $USER->email;
        $to = "houessinonlandry@gmail.com";
        $mail->isSmtp();
        //$mail->Host = "smtp.gmail.com";
        $mail->Host = "email-smtp.eu-west-3.amazonaws.com";
        $mail->Port = 587;
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        $mail->SMTPAuth = true;
        //$mail->Username = 'infoiso.learning@gmail.com';
        $mail->Username = 'AKIAVL5CVGNKC76NW536';
        $mail->Password = 'BGntditQBfy659RfN7F8QRtO53EST3KN2dYge/vlEPla';

        //$mail->Password = "@Azerty21";
        //$mail->setFrom("noreply@learning.iso.org", "NoReply");
        //$mail->setFrom("noreply@learning.iso.org", "NoReply");
        $mail->setFrom("noreply@learning-amaris.com", "NoReply");
        //$mail->setFrom("infoiso.learning@gmail.com", "noreply@learning.iso.org");
        $mail->addReplyTo("noreply@learning-amaris.com", "Do not reply to this email");
        //$mail->SMTPDebug = true;
        $mail->addAddress($to, $USER->firstname.' '.$USER->lastname);
        $mail->Subject = "Amaris Consulting Academy : vos codes d'accès e-learning";
        //$mail->Subject = "$course->fullname;"
        $mail->isHTML(true);
        $mail->CharSet = 'utf-8';

        $mail->Body = "<html><body><h4>Bonjour et bienvenue! , </h4><p>Suite à votre inscription au cours «Référent Sanitaire», vous pouvez désormais vous connecter à notre plateforme à l'aide des codes suivants :</p>
<p><ul><li>Adresse: <a href='https://learning-amaris.com/'>https://learning-amaris.com/</a></li><li>Nom d'utilisateur: ". substr($USER->firstname, 0, 1) .$USER->lastname."</li><li>Mot de passe Initial: #".ucfirst($USER->firstname).ucfirst($USER->lastname)."2020</li></ul></p>
<p>Le mot de passe peut être changé lors de votre première connexion.</p><p>Si vous l'avez déjà modifié et l'avez oublié, veuillez cliquer sur le lien <a href='https://learning-amaris.com/login/forgot_password.php'>mot de passe oublié.</a></p><br><br>
<p>Notre équipe est disponible du lundi au vendredi, de 9h00 à 18h00 par <a href='mailto:egranara@mantu.com'>mail</a><br><br><br><br></p><p>Cordialement,</p><br><br><br>
<p>L’équipe Amaris Consulting Academy </p>
</body></html>";

        //$mail->Body = "<html><body><h1>je teste les mails</h1></body></html>";

        if($mail->send()) {
            // Success! Redirect to a thank you page. Use the
            // POST/REDIRECT/GET pattern to prevent form resubmissions
            // when a user refreshes the page.

            echo "<script>alert('Un mail vous a été envoye ! Vérifier votre boîte mail '); window.location.href='http://localhost:8888/learning-amaris?redirect=0';</script>";
            //header('Location: http://localhost:8888/learning-amaris?redirect=0');
        }
        else {
            //var_dump("Error: " . $mail->ErrorInfo);
            echo '';
        }
    }
    catch (Exception $e) {
        echo '';
    }
}

$urlparams = array();
if (!empty($CFG->defaulthomepage) && ($CFG->defaulthomepage == HOMEPAGE_MY) && optional_param('redirect', 1, PARAM_BOOL) === 0) {
    $urlparams['redirect'] = 0;
}
$PAGE->set_url('/', $urlparams);
$PAGE->set_pagelayout('frontpage');
$PAGE->set_other_editing_capability('moodle/course:update');
$PAGE->set_other_editing_capability('moodle/course:manageactivities');
$PAGE->set_other_editing_capability('moodle/course:activityvisibility');

// Prevent caching of this page to stop confusion when changing page after making AJAX changes.
$PAGE->set_cacheable(false);

require_course_login($SITE);

$hasmaintenanceaccess = has_capability('moodle/site:maintenanceaccess', context_system::instance());

// If the site is currently under maintenance, then print a message.
if (!empty($CFG->maintenance_enabled) and !$hasmaintenanceaccess) {
    print_maintenance_message();
}

$hassiteconfig = has_capability('moodle/site:config', context_system::instance());

if ($hassiteconfig && moodle_needs_upgrading()) {
    redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
}

// If site registration needs updating, redirect.
\core\hub\registration::registration_reminder('/index.php');

if (get_home_page() != HOMEPAGE_SITE) {
    // Redirect logged-in users to My Moodle overview if required.
    $redirect = optional_param('redirect', 1, PARAM_BOOL);
    if (optional_param('setdefaulthome', false, PARAM_BOOL)) {
        set_user_preference('user_home_page_preference', HOMEPAGE_SITE);
    } else if (!empty($CFG->defaulthomepage) && ($CFG->defaulthomepage == HOMEPAGE_MY) && $redirect === 1) {
        redirect($CFG->wwwroot .'/my/');
    } else if (!empty($CFG->defaulthomepage) && ($CFG->defaulthomepage == HOMEPAGE_USER)) {
        $frontpagenode = $PAGE->settingsnav->find('frontpage', null);
        if ($frontpagenode) {
            $frontpagenode->add(
                get_string('makethismyhome'),
                new moodle_url('/', array('setdefaulthome' => true)),
                navigation_node::TYPE_SETTING);
        } else {
            $frontpagenode = $PAGE->settingsnav->add(get_string('frontpagesettings'), null, navigation_node::TYPE_SETTING, null);
            $frontpagenode->force_open();
            $frontpagenode->add(get_string('makethismyhome'),
                new moodle_url('/', array('setdefaulthome' => true)),
                navigation_node::TYPE_SETTING);
        }
    }
}

// Trigger event.
course_view(context_course::instance(SITEID));

$PAGE->set_pagetype('site-index');
$PAGE->set_docs_path('');
$editing = $PAGE->user_is_editing();
$PAGE->set_title($SITE->fullname);
$PAGE->set_heading($SITE->fullname);
$courserenderer = $PAGE->get_renderer('core', 'course');
echo $OUTPUT->header();

$siteformatoptions = course_get_format($SITE)->get_format_options();
$modinfo = get_fast_modinfo($SITE);
$modnamesused = $modinfo->get_used_module_names();

// Print Section or custom info.
if (!empty($CFG->customfrontpageinclude)) {
    // Pre-fill some variables that custom front page might use.
    $modnames = get_module_types_names();
    $modnamesplural = get_module_types_names(true);
    $mods = $modinfo->get_cms();

    include($CFG->customfrontpageinclude);

} else if ($siteformatoptions['numsections'] > 0) {
    echo $courserenderer->frontpage_section1();
}
// Include course AJAX.
include_course_ajax($SITE, $modnamesused);

//TODO ici on touche a la frontpage
echo $courserenderer->frontpage();

if ($editing && has_capability('moodle/course:create', context_system::instance())) {
    echo $courserenderer->add_new_course_button();
}
echo $OUTPUT->footer();
